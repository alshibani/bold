$(document).ready(function(){
    console.log('Bold Interactive scripts initialized.');
    
    /*
    $('#menubar').on('click', '.logout-button', function(){
        $.ajax({
            type: 'POST',
            url: '/admin/login/logout',
            dataType: "json",
            success: function(d){
                if(d.success) window.location.reload();
            }
        });
    });
    */
    
    // Buttons click action
    $('body').on('click', 'button.click-action', function(e) {
        e.preventDefault();
        
        window.location.href = $(this).attr('formaction');
    });
    $('body').on('click', 'button.click-action-confirm', function(e) {
        e.preventDefault();
        
        var check = confirm('Opravdu chcete pokračovat?');
        if(check) {
            window.location.href = $(this).attr('formaction');
        }
    });
    
    $('body').on('click', '.confirm', function(e) {
        e.preventDefault(); 

        var check = confirm('Opravdu chcete pokračovat?');
        if(check == true) {
            return true;
        }
        else {
            return false;
        }
    });

    /*$('.edit-user').click(function(e){
        e.preventDefault();

        var uid = $(this).data('uid');
        window.location.href = '/admin/users/user/'+uid+'/';
    });*/
    
    $('.delete-user').click(function(e){
        e.preventDefault();

        var uid = $(this).data('uid');
        window.location.href = '/admin/users/delete/'+uid+'/';
    });
});