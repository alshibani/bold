<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  Ion Auth Model
 *
 * Author:  Ben Edmunds
 * 		   ben.edmunds@gmail.com
 *	  	   @benedmunds
 *
 * Added Awesomeness: Phil Sturgeon
 *
 * Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
 *
 * Created:  10.01.2009
 *
 * Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 */


class Products_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get product by product_id
     */
    function get_product($product_id)
    {
        return $this->db->get_where('products',array('product_id'=>$product_id))->row_array();
    }

    /*
     * Get all products
     */
    function get_all_products()
    {
        $this->db->order_by('product_id', 'desc');
        return $this->db->get('products')->result_array();
    }

    /*
     * function to add new product
     */
    function add_product($params)
    {
        $this->db->insert('products',$params);
        return $this->db->insert_id();
    }

    function update($product_id,$params)
    {
        $this->db->where('product_id',$product_id);
        return $this->db->update('products',$params);
    }

    /*
     * function to delete product
     */
    function delete_product($product_id)
    {
        return $this->db->delete('products',array('product_id'=>$product_id));

    }
}