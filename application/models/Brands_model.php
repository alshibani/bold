<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Name:  Ion Auth Model
 *
 * Author:  Ben Edmunds
 *           ben.edmunds@gmail.com
 * @benedmunds
 *
 * Added Awesomeness: Phil Sturgeon
 *
 * Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
 *
 * Created:  10.01.2009
 *
 * Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 */
class Brands_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get brand by brand_id
     */
    function get_brand($brand_id)
    {
        return $this->db->get_where('brands', array('brand_id' => $brand_id))->row_array();
    }

    /*
     * Get all brands
     */
    function get_all_brands()
    {
        $this->db->order_by('brand_id', 'desc');
        return $this->db->get('brands')->result_array();
    }

    /*
     * function to add new brand
     */
    function add_brand($params)
    {
        $this->db->insert('brands', $params);
        return $this->db->insert_id();
    }

    function update($brand_id, $params)
    {
        $this->db->where('brand_id', $brand_id);
        return $this->db->update('brands', $params);
    }

    /*
     * function to delete brand
     */
    function delete_brand($brand_id)
    {
        return $this->db->delete('brands', array('brand_id' => $brand_id));
    }

    function getCategoryTree($level = null, $prefix = '-', $ignoreCategory = false)
    {
        $rows = $this->db
            ->select('brand_id,parent_id,brand_name')
            ->where('parent_id', $level)
            ->order_by('brand_id', 'asc')
            ->get('brands')
            ->result();
        $brand = array();
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                if ($row->brand_id != $ignoreCategory) {
                    $brand[] = array($row->brand_id => $prefix . ' ' . $row->brand_name);
                    $brand = array_merge($brand, $this->getCategoryTree($row->brand_id, $prefix . '--',$ignoreCategory));
                }
            }
        }
        return $brand;
    }
}