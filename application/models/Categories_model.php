<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Name:  Ion Auth Model
 *
 * Author:  Ben Edmunds
 *           ben.edmunds@gmail.com
 * @benedmunds
 *
 * Added Awesomeness: Phil Sturgeon
 *
 * Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
 *
 * Created:  10.01.2009
 *
 * Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 */
class Categories_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get category by category_id
     */
    function get_category($category_id)
    {
        return $this->db->get_where('categories', array('category_id' => $category_id))->row_array();
    }

    /*
     * Get all categories
     */
    function get_all_categories()
    {
        $this->db->order_by('category_id', 'desc');
        return $this->db->get('categories')->result_array();
    }

    /*
     * function to add new category
     */
    function add_category($params)
    {
        $this->db->insert('categories', $params);
        return $this->db->insert_id();
    }

    function update($category_id, $params)
    {
        $this->db->where('category_id', $category_id);
        return $this->db->update('categories', $params);
    }

    /*
     * function to delete category
     */
    function delete_category($category_id)
    {
        return $this->db->delete('categories', array('category_id' => $category_id));
    }

    function getCategoryTree($level = null, $prefix = '-', $ignoreCategory = false)
    {
        $rows = $this->db
            ->select('category_id,parent_id,category_name')
            ->where('parent_id', $level)
            ->order_by('category_id', 'asc')
            ->get('categories')
            ->result();
        $category = array();
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                if ($row->category_id != $ignoreCategory) {
                    $category[] = array($row->category_id => $prefix . ' ' . $row->category_name);
                    $category = array_merge($category, $this->getCategoryTree($row->category_id, $prefix . '--',$ignoreCategory));
                }
            }
        }
        return $category;
    }
}