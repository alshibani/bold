<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Admin_Controller
{

	function __construct()
	{
	  parent::__construct();
	  $this->load->helper('form');
	}

	public function index()
	{

		if($this->ion_auth->user()->row()) { 
			if($this->ion_auth->in_group(1)) {
				redirect('/admin/dashboard');
			} elseif($this->ion_auth->in_group(2)) {
				redirect('/admin/dashboard');
			} elseif($this->ion_auth->in_group(3)) {
				redirect('/platform/dashboard');
			} elseif($this->ion_auth->in_group(4)) {
				redirect('/platform2/dashboard');
			} else {
                redirect('/');
            }
		}

		$this->load->view('admin/auth/login');

		if(!empty($_POST)){
			$username = $this->input->post('username');
			$pw = $this->input->post('pw');
			$remember = $this->input->post('remember');

			if($this->ion_auth->login($username, $pw, $remember)) {
                if($this->ion_auth->in_group(1)) {
                    redirect('/admin/dashboard');
                } elseif($this->ion_auth->in_group(2)) {
                    redirect('/admin/dashboard');
                } elseif($this->ion_auth->in_group(3)) {
                    redirect('/platform/dashboard');
                } elseif($this->ion_auth->in_group(4)) {
                    redirect('/platform2/dashboard');
                } else {
					redirect('/');
				}
			}
		}
	}

	public function logout()
	{
		if($this->ion_auth->logout()){
            redirect('/admin/login');
		}
	}
}
