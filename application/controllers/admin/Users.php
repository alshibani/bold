<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller
{

	function __construct()
	{
	  parent::__construct();
	  $this->load->helper('form');
	}

	public function index()
	{
        // Data
        $data['pagetitle'] = 'Správa uživatelů';
        
        $data['users'] = $this->users_list();
        
        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/users', $data);

        $this->load->view('admin/layout/footer');
    }
    
    public function users_json()
    {
        $users = $this->users_list();
        $data = array('data' => array());
        $i = 0;
        foreach($users as $user) {
            $data['data'][$i][0] = $user->first_name.' '.$user->last_name;
            $data['data'][$i][1] = $user->role;
            $data['data'][$i][2] = $user->email;
            $data['data'][$i][3] = $user->id;
            $i++;
        }
        
        echo json_encode($data);
    }

    public function delete($id)
    {
        $usertodelete = $this->db->where('id', $id)->get('users')->row();
        if(($usertodelete)&&($this->ion_auth->in_group(1))) {
            if($this->db->where('id', $id)->delete('users')) {
                $this->session->set_flashdata('success', 'Uživatel byl úspěšně smazán.');
                redirect('admin/users/');
            }
        } else {
            $this->session->set_flashdata('error', 'Uživatel neexistuje, nebo nemáte dostatečné oprávnění.');
            redirect('admin/users/');            
        }
    }
    
    public function detail($id)
    {
        // Data
        $data['pagetitle'] = 'Detail uživatele';

        $user_in_view = $this->db->where('id', $id)->get('users')->row();
        $roles = $this->db->select('groups.name as name, groups.id as group_id')->from('users_groups')->where('user_id', $user_in_view->id)->join('groups', 'groups.id = users_groups.group_id')->get()->row();

        $data['user'] = $user_in_view;
        $data['user']->role = ucfirst($roles->name);
        $data['user']->roleid = $roles->group_id;

        $roles = $this->db->where('description', 'admin')->get('groups')->result();
        $roles_result = array();
        foreach($roles as $role) {
            $roles_result[$role->id] = ucfirst($role->name);
        }
        $data['roles'] = $roles_result;
        
        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');        

        $this->load->view('admin/user-detail', $data);
        
        $this->load->view('admin/layout/footer');

        // Actions
		if(!empty($_POST['submit_edit_user'])){
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $newrole = $this->input->post('role');
            $email = $this->input->post('email');
            
            $update = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email
            );

			if($this->ion_auth->update($id, array('first_name' => $first_name, 'last_name' => $last_name, 'email' => $email))) {
                if($newrole != $role->id) {
                    $this->db->where('user_id', $id)->update('users_groups', array('group_id' => $newrole));
                    $this->session->set_flashdata('success', 'Změny úspěšně uloženy.');
                }
                redirect('admin/users/');
			} else {
                $this->session->set_flashdata('error', 'Změny se nepodařilo uložit.');
                redirect('admin/users/');
            }
		}

    }

	public function add()
	{
        // Data
        $data['pagetitle'] = 'Add a user';

        $roles = $this->db->where('description', 'admin')->get('groups')->result();
        $roles_result = array();
        foreach($roles as $role) {
            $roles_result[$role->id] = ucfirst($role->name);
        }
        $data['roles'] = $roles_result;
        
        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/user-add', $data);
        
        $this->load->view('admin/layout/footer');

        // Actions
		if(!empty($_POST['submit_add_user'])){
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $role = $this->input->post('role');
			$password = $this->input->post('password');
            $email = $this->input->post('email');

			if($this->ion_auth->register($email, $password, $email, array('first_name' => $first_name, 'last_name' => $last_name), array($role))){
                $this->session->set_flashdata('success', 'Uživatel byl úspěšně přidán.');
                redirect('admin/users/');
			} else {
                $this->session->set_flashdata('error', 'Chyba při přidání uživatele.');
                redirect('admin/users/');
            }
		}
	}
    
    
    // Private methods
    
    private function users_list() {
        // Get users list with role
        $users = $this->db->get('users')->result();
        foreach($users as $user) {
            $roles = $this->db->select('name')->from('users_groups')->where('user_id', $user->id)->join('groups', 'groups.id = users_groups.group_id')->get()->row();
            $user->role = ucfirst($roles->name);
        }
        return $users;
    }
}
