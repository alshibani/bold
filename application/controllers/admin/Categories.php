<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
    }


    public function index()
    {
        // Data
        $data['pagetitle'] = 'Správa uživatelů';

        $data['categories'] = $this->categories_list();

        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/categories/category', $data);

        $this->load->view('admin/layout/footer');
    }


    public function categories_json()
    {
        $categories = $this->categories_list();
        $data = array('data' => array());
        $i = 0;
        foreach ($categories as $category) {
            $data['data'][$i][] = $category->category_name;
            $data['data'][$i][] = $category->parent_name ? $category->parent_name : '';
            $data['data'][$i][] = $category->category_id;
            $i++;
        }

        echo json_encode($data);
    }


    public function delete($id)
    {
        // @todo: updte product categories to null
        $categoryToDelete = $this->db->where('category_id', $id)->get('categories')->row();
        if ($categoryToDelete) {
            if ($this->db->where('category_id', $id)->delete('categories')) {
                $this->session->set_flashdata('success', 'Category deleted successfully');
                redirect('admin/categories/');
            }
        } else {
            $this->session->set_flashdata('error', 'Unable to delete the category.');
            redirect('admin/categories/');
        }
    }


    public function detail($id)
    {
        $this->load->model('Categories_model');

        // Data
        $data['pagetitle'] = 'Category Details';

        $category = $this->db->select("categories.*, parent.category_name as parent_name")
            ->where('categories.category_id', $id)
            ->from('categories')->join('categories as parent', 'parent.category_id = categories.parent_id', 'left')
            ->order_by('categories.parent_id', 'asc')->get()->row();

        if (!$category) {
            show_404();
        }


        $data['category'] = $category;
        $this->load->model('Categories_model');
        $data['categoriesList'] = $this->Categories_model->getCategoryTree(null, '-', $id);

        $this->getFormValidation();

        // Actions
        if (!empty($_POST['submit_edit_category'])) {

            if ($this->form_validation->run()) {

                $update = array(
                    'parent_id' => $this->input->post('parent_id') === '' ? null : $this->input->post('parent_id'), // parent_id or null. no empty strings allowed.
                    'category_name' => $this->input->post('category_name'),
                );

                if ($this->Categories_model->update($id, $update)) {
                    $this->session->set_flashdata('success', 'Category updated successfully.');
                    redirect('admin/categories/');
                } else {
                    $this->session->set_flashdata('error', 'Unable to save changes.');
                    redirect('admin/categories/');
                }
            }
        }

        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/categories/category-detail', $data);

        $this->load->view('admin/layout/footer');

    }


    public function add()
    {
        $this->load->model('Categories_model');

        // Data
        $data['pagetitle'] = 'Category Details';

        $this->load->model('Categories_model');
        $data['categoriesList'] = $this->Categories_model->getCategoryTree();

        $this->getFormValidation();

        // Actions
        if (!empty($_POST['submit_add_category'])) {
            if ($this->form_validation->run()) {

                $update = array(
                    'parent_id' => $this->input->post('parent_id') === '' ? null : $this->input->post('parent_id'), // parent_id or null. no empty strings allowed.
                    'category_name' => $this->input->post('category_name'),
                );

                if ($this->Categories_model->add_category($update)) {
                    $this->session->set_flashdata('success', 'Category added successfully.');
                    redirect('admin/categories/');
                } else {
                    $this->session->set_flashdata('error', 'Unable to add category.');
                    redirect('admin/categories/');
                }
            }
        }

        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/categories/category-add', $data);

        $this->load->view('admin/layout/footer');
    }


    // Private methods

    private function getFormValidation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('category_name', 'Category Name', 'required');
    }

    private function categories_list()
    {
        // Get categories list with category
        $categories = $this->db->select("categories.*, parent.category_name as parent_name")->from('categories')->join('categories as parent', 'parent.category_id = categories.parent_id', 'left')->order_by('categories.parent_id', 'asc')->get()->result();
//        echo $this->db->last_query();
        return $categories;
    }

}
