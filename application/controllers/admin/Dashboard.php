<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {

    $data['pagetitle'] = 'Dashboard';

    $this->load->view('admin/layout/header', $data);
    $this->load->view('admin/layout/menu');
    $this->load->view('admin/layout/footer');
  }
}