<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
    }

    public function index()
    {
        // Data
        $data['pagetitle'] = 'products list';

        $data['products'] = $this->products_list();

        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/products/product', $data);

        $this->load->view('admin/layout/footer');
    }

    public function lists()
    {

        $this->load->model('Products_model');
        $this->load->model('Categories_model');

        // Data
        $data['pagetitle'] = 'Product search';


        // query products from db

        $filters = array();
        if (!empty($_GET['create_date'])) {
            $filters['create_date   >'] = $_GET['create_date'];
        }

        $query = $this->db->where($filters)->get('products');
        $result = $query->result();
//        echo $this->db->last_query();exit;
        $data['products'] = $result;

        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/products/product_lists', $data);

        $this->load->view('admin/layout/footer');
    }

    public function products_json()
    {
        $products = $this->products_list();
        $data = array('data' => array());
        $i = 0;

        foreach ($products as $product) {
            $data['data'][$i][] = $product->product_name;
            $data['data'][$i][] = $product->category_name ? $product->category_name : '';
            $data['data'][$i][] = $product->purchase_price;
            $data['data'][$i][] = $product->wholesale_price;
            $data['data'][$i][] = $product->retail_price;
            $data['data'][$i][] = $product->items_in_stock;
            $data['data'][$i][] = $product->create_date;
            $data['data'][$i][] = $product->product_id;
            $i++;
        }

        echo json_encode($data);
    }

    public function delete($id)
    {
        $productToDelete = $this->db->where('product_id', $id)->get('products')->row();

        if ($productToDelete) {
            $photos = $this->db->from('photo')->where('product_id', $id)->get()->result();
//            echo $this->db->last_query();exit;
            foreach ($photos as $photo) {
//                var_export($photo);exit;
                $this->unlinkPhoto($photo);
            }

            if ($this->db->where('product_id', $id)->delete('products')) {
                $this->session->set_flashdata('success', 'Product deleted successfully');
                redirect('admin/products/');
            }
        } else {
            $this->session->set_flashdata('error', 'Unable to delete the product.');
            redirect('admin/products/');
        }
    }


    public function upload($id)
    {
        $config['upload_path'] = realpath(FCPATH . "uploads/") . DIRECTORY_SEPARATOR;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024 * 1024;
        $config['encrypt_name'] = TRUE;
//        $config['max_width'] = 1024;
//        $config['max_height'] = 768;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            header("HTTP/1.0 400 Bad Request");
            echo $this->upload->display_errors();
        } else {
            $data = array('upload_data' => $this->upload->data());
            $result = $this->db->insert('photo', array(
                'product_id' => $id,
                'photo_file' => $data['upload_data']['file_name'],
                'photo_title' => $data['upload_data']['orig_name']
            ));
            return $result;
        }
    }

    public function gallery($id)
    {
        $this->load->model('Products_model');

        // Data
        $data['pagetitle'] = 'Product Details';

        $product = $this->db->from('products')
            ->join('categories', 'products.category_id = categories.category_id', 'left')
            ->where('product_id', $id)
            ->get()->row();
//
        if (!$product) {
            show_404();
        }

        $photos = $this->db->from('photo')->where('product_id', $id)->get()->result();
//        echo $this->db->last_query();exit;
        $data['product'] = $product;
        $data['product']->photos = $photos;

        // Actions
        if ($this->input->post('photo_id')) {
            $photo_id = $this->input->post('photo_id');
            $photoToDelete = $this->db->where('photo_id', $photo_id)->get('photo')->row();
            if ($photoToDelete) {

                // Remove the file from uploads folder:
                $this->unlinkPhoto($photoToDelete);

                $result = $this->db->where('photo_id', $photo_id)->delete('photo');

                if ($result) {
                    // todo: photos here
                    $this->session->set_flashdata('success', 'The photo deleted successfully.');
                    redirect("admin/products/gallery/{$id}");
                } else {
                    $this->session->set_flashdata('error', 'Unable to save changes.');
                    redirect("admin/products/gallery/{$id}");
                }
            }
        }

        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/products/product-gallery', $data);

        $this->load->view('admin/layout/footer');

    }


    public function detail($id)
    {
        $this->load->model('Products_model');

        // Data
        $data['pagetitle'] = 'Product Details';

        $product = $this->db->from('products')
            ->join('categories', 'products.category_id = categories.category_id', 'left')
            ->where('product_id', $id)
            ->get()->row();

        if (!$product) {
            show_404();
        }

        $photos = $this->db->from('photo')->where('product_id', $id)->get()->result();


        $data['product'] = $product;
        $data['product']->photos = $photos;

        $this->load->model('Categories_model');
        $data['categoriesList'] = $this->Categories_model->getCategoryTree();


        $this->getFormValidation();

        // Actions
        if (!empty($_POST['submit_edit_product'])) {

            if ($this->form_validation->run()) {


                $update = array(
                    'category_id' => $this->input->post('category_id') === '' ? null : $this->input->post('category_id'), // category id or null. no empty strings allowed.
                    'product_name' => $this->input->post('product_name'),
                    'purchase_price' => $this->input->post('purchase_price'),
                    'wholesale_price' => $this->input->post('wholesale_price'),
                    'retail_price' => $this->input->post('retail_price'),
                    'ean' => $this->input->post('ean'),
                    'items_in_stock' => $this->input->post('items_in_stock'),
                    'product_descrption' => $this->input->post('product_descrption'),
                    'location' => $this->input->post('location'),
                );

                if ($this->Products_model->update($id, $update)) {
                    $this->session->set_flashdata('success', 'Product updated successfully.');
                    redirect('admin/products/');
                } else {
                    $this->session->set_flashdata('error', 'Unable to save changes.');
                    redirect('admin/products/');
                }
            }
        }

        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/products/product-detail', $data);

        $this->load->view('admin/layout/footer');

    }

    public function add()
    {
        $this->load->model('Products_model');
        $this->load->model('Categories_model');

        // Data
        $data['pagetitle'] = 'Product Details';


        $data['categoriesList'] = $this->Categories_model->getCategoryTree();

        $this->getFormValidation();

        // Actions
        if (!empty($_POST['submit_add_product'])) {

            if ($this->form_validation->run()) {
//                var_export("ssdds");exit;
                $update = array(
                    'category_id' => $this->input->post('category_id') === '' ? null : $this->input->post('category_id'), // category id or null. no empty strings allowed.
                    'product_name' => $this->input->post('product_name'),
                    'purchase_price' => $this->input->post('purchase_price'),
                    'wholesale_price' => $this->input->post('wholesale_price'),
                    'retail_price' => $this->input->post('retail_price'),
                    'ean' => $this->input->post('ean'),
                    'items_in_stock' => $this->input->post('items_in_stock'),
                    'product_descrption' => $this->input->post('product_descrption'),
                    'location' => $this->input->post('location'),
                );

                if ($this->Products_model->add_product($update)) {
                    $this->session->set_flashdata('success', 'Product added successfully.');

                    redirect('admin/products/');
                } else {
                    $this->session->set_flashdata('error', 'Unable to add product.');
                    redirect('admin/products/');
                }
            }
            else
            {
//                var_export($this->form_validation->error_array());exit;
            }
        }

        // View
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/products/product-add', $data);

        $this->load->view('admin/layout/footer');
    }


    // Private methods

    private function getFormValidation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('product_name', 'Product Name', 'required');
        $this->form_validation->set_rules('purchase_price', 'Purchase Price', 'required|decimal');
        $this->form_validation->set_rules('wholesale_price', 'Wholesale Price', 'required');
        $this->form_validation->set_rules('retail_price', 'Retail Price', 'required|decimal');
        $this->form_validation->set_rules('ean', 'Ean', 'required');
        $this->form_validation->set_rules('items_in_stock', 'Items In Stock', 'required|integer');

    }

    private function products_list()
    {
        // Get products list with category
        $products = $this->db->from('products')->join('categories', 'products.category_id = categories.category_id', 'left')->get()->result();

//            echo $this->db->last_query();exit;
        return $products;
    }
// delete the photos from the folder
    private function unlinkPhoto($photoToDelete)
    {
        $upload_path = realpath(FCPATH . "uploads/") . DIRECTORY_SEPARATOR;
//var_export($upload_path);exit;
        $photoFile = $upload_path . $photoToDelete->photo_file;
//        var_export($photoFile);exit;
        // Make sure the files is exists:
        if (is_file($photoFile)) {
            @unlink($photoFile);
        }
    }

    private function searchProducts($value)
    {

        var_export("fff");
        exit;
        $this->db->select('*');
        $this->db->from('products');
        $this->db->like('create_date', $value['start_date']);

        // Execute the query.
        $query = $this->db->get();

        // Return the results.
        return $query->result_array();
    }
}
