<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
    }


    public function index()
    {

        $data['pagetitle'] = 'Brands';

        $data['brands'] = $this->brand_list();

        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/brands/brand', $data);

        $this->load->view('admin/layout/footer');

    }

    public function brands_json()
    {
        $brands = $this->brand_list();
        $data = array('data' => array());
        $i = 0;
        foreach ($brands as $brand) {
            $data['data'][$i][] = $brand->brand_name;
            $data['data'][$i][] = $brand->brand_id;
            $i++;
        }

        echo json_encode($data);
    }

    public function add()
    {

        $this->load->model('brands_model');

        $data['pagetitle'] = 'Brand Details';

        $this->form_validation();

        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/layout/menu');
        $this->load->view('admin/layout/alerts');

        $this->load->view('admin/brands/brand-add', $data);

        $this->load->view('admin/layout/footer');


    }


// private methods

    private function brand_list()
    {
        $brands = $this->db->from('brands')
            ->join('products', 'products.brand_id=brands.brand_id', 'left')
            ->get()->result();
//        echo $this->db->last_query();
        return $brands;
    }

    private function form_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('brand_name', 'Brand name', 'required');
    }

}