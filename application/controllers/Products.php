<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Public_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['pagetitle'] = 'Product Details';

        $data['products'] = $this->products_list();

        $this->load->view('public/layout/header', $data);
        $this->load->view('public/layout/menu');
        $this->load->view('public/products-index', $data);
        $this->load->view('public/layout/footer');
    }


    public function detail($id)
    {
        $product = $this->db->from('products')
            ->where('product_id', $id)
            ->join('categories', 'products.category_id = categories.category_id', 'left')
            ->get()->row();

        if (!$product) {
            show_404();
        }

        $data['product'] = $product;

        $photos = $this->db->from('photo')->where('product_id', $id)->get()->result();

        $data['product']->photos = $photos;

        $this->load->view('public/layout/header', $data());
        $this->load->view('public/layout/menu');
        $this->load->view('public/products-detail', $data);
        $this->load->view('public/layout/footer');
    }


    private function products_list()
    {
        // Get products list with category
        $products = $this->db->from('products')
            ->join('categories', 'products.category_id = categories.category_id', 'left')
            ->join('photo','products.product_id =photo.product_id', 'left')->group_by('products.product_id')
            ->get()->result_array();

        return $products;
    }




}