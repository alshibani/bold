<div class="row">
    <div class="col-md-6">
        <h4 class="m-b-lg">Product Details</h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" formaction="<?php echo base_url(); ?>admin/products/"
                class="btn btn-sm btn-primary click-action text-right"><i class="fa fa-angle-left"></i> &nbsp; Back
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="widget p-lg">

            <div class="media">
                <div class="media-left">
                    <div class="avatar avatar-lg avatar-circle">
                        <img class="img-responsive" src="http://via.placeholder.com/150" alt="avatar">
                    </div><!-- .avatar -->
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo $product->product_name ?></h4>
                    <small class="media-meta"><?php echo $product->category_name ?></small>
                    <small class="media-meta text-warning"><?php echo $product->purchase_price ?></small>
                </div>
                <?php if ($product->photos): ?>
                    <h5>Gallery:</h5>
                    <!-- Image Gallery -->
                    <div class="gallery row">
                        <?php foreach ($product->photos as $photo): ?>
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                <div class="gallery-item">
                                    <div class="thumb">
                                        <a href="<?php echo base_url('uploads/') . $photo->photo_file ?>"
                                           data-lightbox="gallery-2"
                                           data-title="<?php echo $photo->photo_title ?>">
                                            <img class="img-responsive"
                                                 src="<?php echo base_url('uploads/') . $photo->photo_file ?>"
                                                 alt="<?php echo $photo->photo_title ?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div><!-- END .gallery -->
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Detail uživatele</h4>
            <div class="table-responsive">
                <?php echo form_open() ?>

                <div class="form-group">
                    <label for="category_id">Category</label>

                    <select name="category_id" class="form-control">
                        <option value="">select category</option>
                        <?php
                        foreach ($categoriesList as $cat) {
                            $cat_id = key($cat);
                            $cat_name = $cat[$cat_id];
                            $selected = ($cat_id == $product->category_id) ? ' selected="selected"' : "";

                            echo '<option value="' . $cat_id . '" ' . $selected . '>' . $cat_name . '</option>';
                        }
                        ?>
                    </select>

                </div>
                <div class="form-group">
                    <label for="product_name">Product Name *</label>
                    <input type="text" name="product_name"
                           value="<?php echo($this->input->post('product_name') ? $this->input->post('product_name') : $product->product_name); ?>"
                           class="form-control" id="product_name"/>
                    <span class="text-danger"><?php echo form_error('product_name'); ?></span>
                </div>
                <div class="form-group">
                    <label for="purchase_price">Purchase Price *</label>
                    <input type="text" name="purchase_price"
                           value="<?php echo($this->input->post('purchase_price') ? $this->input->post('purchase_price') : $product->purchase_price); ?>"
                           class="form-control" id="purchase_price"/>
                    <span class="text-danger"><?php echo form_error('purchase_price'); ?></span>
                </div>
                <div class="form-group">
                    <label for="wholesale_price">Wholesale Price *</label>

                    <input type="text" name="wholesale_price"
                           value="<?php echo($this->input->post('wholesale_price') ? $this->input->post('wholesale_price') : $product->wholesale_price); ?>"
                           class="form-control" id="wholesale_price"/>
                    <span class="text-danger"><?php echo form_error('wholesale_price'); ?></span>

                </div>
                <div class="form-group">
                    <label for="retail_price">Retail Price *</label>

                    <input type="text" name="retail_price"
                           value="<?php echo($this->input->post('retail_price') ? $this->input->post('retail_price') : $product->retail_price); ?>"
                           class="form-control" id="retail_price"/>
                    <span class="text-danger"><?php echo form_error('retail_price'); ?></span>

                </div>
                <div class="form-group">
                    <label for="ean">Ean *</label>

                    <input type="text" name="ean"
                           value="<?php echo($this->input->post('ean') ? $this->input->post('ean') : $product->ean); ?>"
                           class="form-control" id="ean"/>
                    <span class="text-danger"><?php echo form_error('ean'); ?></span>

                </div>
                <div class="form-group">
                    <label for="items_in_stock">Items In Stock *</label>

                    <input type="text" name="items_in_stock"
                           value="<?php echo($this->input->post('items_in_stock') ? $this->input->post('items_in_stock') : $product->items_in_stock); ?>"
                           class="form-control" id="items_in_stock"/>
                    <span class="text-danger"><?php echo form_error('items_in_stock'); ?></span>

                </div>
                <div class="form-group">
                    <label for="product_descrption">Product Descrption</label>

                    <textarea name="product_descrption" class="form-control"
                              id="product_descrption"><?php echo($this->input->post('product_descrption') ? $this->input->post('product_descrption') : $product->product_descrption); ?></textarea>

                </div>
                <div class="form-group">
                    <label for="location">Location</label>

                    <textarea name="location" class="form-control"
                              id="location"><?php echo($this->input->post('location') ? $this->input->post('location') : $product->location); ?></textarea>

                </div>

                <button type="submit" name="submit_edit_product" value="1" class="btn btn-primary btn-md">Save changes
                </button>
                </form>
            </div>
        </div>
    </div>
