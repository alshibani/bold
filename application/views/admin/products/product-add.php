<div class="row">
    <div class="col-md-6">
        <h4 class="m-b-lg">Product</h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" formaction="<?php echo base_url(); ?>admin/products/" class="btn btn-sm btn-primary click-action text-right"><i class="fa fa-angle-left"></i> &nbsp; back</button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Add a new product</h4>

            <?php echo form_open() ?>
                <div class="table-responsive">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="category_id" >Category</label>

                            <select name="category_id" class="form-control">
                                <option value="">select category</option>
                                <?php
                                foreach($categoriesList as $cat)
                                {
                                    $cat_id = key($cat);
                                    $cat_name = $cat[$cat_id];
                                    $selected = ($cat_id == $this->input->post('category_id')) ? ' selected="selected"' : "";
                                    echo '<option value="'.$cat_id.'" '.$selected.'>'.$cat_name.'</option>';
                                }
                                ?>
                            </select>

                        </div>
                        <div class="form-group">
                            <label for="product_name" >Product Name *</label>
                            <input type="text" name="product_name"
                                   value="<?php echo $this->input->post('product_name'); ?>" class="form-control" id="product_name" />
                            <span class="text-danger"><?php echo form_error('product_name');?></span>
                        </div>
                        <div class="form-group">
                            <label for="purchase_price" >Purchase Price *</label>
                            <input type="text" name="purchase_price"
                                   value="<?php echo $this->input->post('purchase_price'); ?>" class="form-control" id="purchase_price" />
                            <span class="text-danger"><?php echo form_error('purchase_price');?></span>
                        </div>
                        <div class="form-group">
                            <label for="wholesale_price" >Wholesale Price *</label>

                            <input type="text" name="wholesale_price"
                                   value="<?php echo $this->input->post('wholesale_price'); ?>" class="form-control" id="wholesale_price" />
                            <span class="text-danger"><?php echo form_error('wholesale_price');?></span>

                        </div>
                        <div class="form-group">
                            <label for="retail_price" >Retail Price *</label>

                            <input type="text" name="retail_price"
                                   value="<?php echo $this->input->post('retail_price'); ?>" class="form-control" id="retail_price" />
                            <span class="text-danger"><?php echo form_error('retail_price');?></span>

                        </div>


                        <button type="submit" name="submit_add_product" value="1" class="btn btn-primary btn-md">Save</button>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ean" >Ean *</label>

                            <input type="text" name="ean"
                                   value="<?php echo $this->input->post('ean'); ?>" class="form-control" id="ean" />
                            <span class="text-danger"><?php echo form_error('ean');?></span>

                        </div>
                        <div class="form-group">
                            <label for="items_in_stock" >Items In Stock *</label>

                            <input type="text" name="items_in_stock"
                                   value="<?php echo $this->input->post('items_in_stock'); ?>" class="form-control" id="items_in_stock" />
                            <span class="text-danger"><?php echo form_error('items_in_stock');?></span>

                        </div>
                        <div class="form-group">
                            <label for="product_descrption" >Product Descrption</label>

                            <textarea name="product_descrption" class="form-control" id="product_descrption"><?php echo $this->input->post('product_descrption'); ?></textarea>

                        </div>
                        <div class="form-group">
                            <label for="location" >Location</label>

                            <textarea name="location" class="form-control" id="location"><?php echo $this->input->post('location'); ?></textarea>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>