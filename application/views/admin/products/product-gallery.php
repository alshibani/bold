<div class="row">
    <div class="col-md-6">
        <h4 class="m-b-lg">Product Gallery</h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" formaction="<?php echo base_url(); ?>admin/products/"
                class="btn btn-sm btn-primary click-action text-right"><i class="fa fa-angle-left"></i> &nbsp; Zpět
        </button>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Upload New Images</h4>
            <?php $uploadUrl = base_url() . "admin/products/upload/{$product->product_id}"; ?>
            <?php echo form_open('', array(
                'class' => 'dropzone',
                'data-plugin' => 'dropzone',
                'data-options' => "{ autoDiscover : false, url: '" . $uploadUrl . "'}"
            )) ?>
            <div class="dz-message">
                <h3 class="m-h-lg">Drop files here or click to upload.</h3>
                <p class="m-b-lg text-muted">(This is just a demo dropzone. Selected files are not actually
                    uploaded.)</p>
            </div>
            </form>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="mail-toolbar m-b-lg">
            <h4 class="m-b-lg">
                Product Images
                <a href="<?php echo base_url(); ?>admin/products/gallery/<?php echo $product->product_id ?>"
                   class="btn btn-default"><i class="fa fa-refresh"></i></a>
            </h4>
        </div>
    </div>
</div>
<?php if ($product->photos): ?>
    <!-- Image Gallery -->
    <div class="gallery row">
        <?php foreach ($product->photos as $photo): ?>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <div class="gallery-item">
                    <div class="thumb">
                        <a href="<?php echo base_url('uploads/') . $photo->photo_file ?>" data-lightbox="gallery-2"
                           data-title="<?php echo $photo->photo_title ?>">
                            <img class="img-responsive" src="<?php echo base_url('uploads/') . $photo->photo_file ?>"
                                 alt="<?php echo $photo->photo_title ?>">
                        </a>
                    </div>
                    <div class="caption">
                        <?php echo form_open() ?>
                        <input type="hidden" name="photo_id" value="<?php echo $photo->photo_id ?>">
                        <button type="submit" class="btn btn-danger rounded btn-xs"><span class="fa fa-remove"></span>
                        </button>
                        </form>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div><!-- END .gallery -->
<?php else: ?>
    <div class="alert alert-warning">
        <div>No existing images, please use the dropzone above to upload new images.</div>
    </div>
<?php endif; ?>