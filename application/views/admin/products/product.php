<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Products List</h4>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Products List</h4>

            <button type="button" formaction="<?php echo base_url(); ?>admin/products/add/" class="btn btn-primary click-action">Add </button>

            <div class="space-25"></div>

            <table id="responsive-datatable" data-plugin="DataTable" data-options="{
                        ajax: base_url+'admin/products/products_json',
                        responsive: true,
                        keys: true,
                        columnDefs: [
                            {
                                'render': function ( data, type, row ) {
                                    return '<button type=\'button\' formaction=\''+base_url+'admin/products/detail/'+data+'\' class=\'btn btn-primary btn-xs edit-product click-action\'>Edit</button>'
                                            + ' <button type=\'button\' formaction=\''+base_url+'admin/products/gallery/'+data+'\' class=\'btn btn-primary btn-xs gallery-product click-action\'>Gallery</button>'
                                            + ' <button type=\'button\' formaction=\''+base_url+'admin/products/delete/'+data+'\' class=\'btn btn-danger btn-xs edit-product click-action-confirm\'>Delete</button>';
                                },
                                'targets': 7
                            },
                            {
                                'render': function ( data, type, row ) {
                                    return '<a href=\''+base_url+'admin/products/detail/'+row[7]+'\'>'+data+'</a>';
                                },
                                'targets': 0
                            }
                        ],
                        aoColumns: [ 
                            {'sClass': 'text-left'},
                            {'sClass': 'text-left'},
                            {'sClass': 'text-left'},
                            {'sClass': 'text-right'}
                        ],
                        language: {
                            'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/English.json'
                        }
                    }" class="table" cellspacing="0" width="100%">
                <thead>
                    <th>Name</th>
                    <th>Category Name</th>
                    <th>Purchase Price</th>
                    <th>Wholesale Price</th>
                    <th>Retail Price</th>
                    <th>Items in Stock</th>
                    <th>Create Date</th>
                    <th></th>
                </thead>
            </table>
        </div>
    </div>
</div>


