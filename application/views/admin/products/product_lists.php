<div class="row">
    <div class="col-md-6">
        <h4 class="m-b-lg">Product</h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" formaction="<?php echo base_url(); ?>admin/products/"
                class="btn btn-sm btn-primary click-action text-right"><i class="fa fa-angle-left"></i> &nbsp; back
        </button>
    </div>
</div>

<?php


?>

<div class="col-md-12">

    <div class="widget p-lg">
        <h4 class="m-b-lg">search result</h4>
        <p class="m-b-lg docs">
        </p>
        <form method="Get">
            <div class="col-md-7 col-md-offset-1">

                <div class="form-group" style="position: relative;">

                    <label for="datetimepicker5">create date</label>

                    <input type="text" name="create_date"
                           id="datetimepicker5" class="form-control" data-plugin="datetimepicker"
                           data-options="{ defaultDate: '3/27/2016' }">
                </div><!-- .form-group -->


                <button type="submit" name="submit_search_product" value="1" class="btn btn-primary btn-md">search
                </button>
            </div>
        </form>
        <table class="table table-striped">

            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Price</th>
                <th>Items in Stock</th>
                <th>Create Date</th>
                <th>Location</th>
            </tr>
            <?php foreach ($products as $row) : ?>

                <tr>
                    <td><?php echo $row->product_name; ?></td>
                    <td><?php echo $row->purchase_price; ?></td>
                    <td><?php echo $row->wholesale_price; ?></td>
                    <td><?php echo $row->items_in_stock; ?></td>
                    <td><?php echo $row->create_date; ?></td>
                    <td><?php echo $row->location; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div><!-- .widget -->
</div><!-- END column -->



