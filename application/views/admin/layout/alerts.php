<div class="row">
    <div class="col-md-12">
        <?php if($this->session->flashdata('success')) { ?>
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>Dokončeno! </strong>
                <span><?php echo $this->session->flashdata('success'); ?></span>
            </div>
        <?php } ?>
        
        <?php if($this->session->flashdata('error')) { ?>
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>Chyba! </strong>
                <span><?php echo $this->session->flashdata('error'); ?></span>
            </div>
        <?php } ?>
    </div>
</div>