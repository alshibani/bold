<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Správa uživatelů</h4>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Aktivní uživatelé</h4>

            <button type="button" formaction="<?php echo base_url(); ?>admin/users/add/" class="btn btn-primary click-action">Add user</button>

            <div class="space-25"></div>

            <table id="responsive-datatable" data-plugin="DataTable" data-options="{
                        ajax: base_url+'admin/users/users_json',
                        responsive: true,
                        keys: true,
                        columnDefs: [
                            {
                                'render': function ( data, type, row ) {
                                    return '<button type=\'button\' formaction=\''+base_url+'admin/users/detail/'+data+'\' class=\'btn btn-primary btn-xs edit-user click-action\'>Upravit</button>'
                                            +' <button type=\'button\' formaction=\''+base_url+'admin/users/delete/'+data+'\' class=\'btn btn-danger btn-xs edit-user click-action-confirm\'>Smazat</button>';
                                },
                                'targets': 3
                            },
                            {
                                'render': function ( data, type, row ) {
                                    return '<a href=\''+base_url+'admin/users/detail/'+row[3]+'\'>'+data+'</a>';
                                },
                                'targets': 0
                            }
                        ],
                        aoColumns: [ 
                            {'sClass': 'text-left'},
                            {'sClass': 'text-left'},
                            {'sClass': 'text-left'},
                            {'sClass': 'text-right'}
                        ],
                        language: {
                            'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Czech.json'
                        }
                    }" class="table" cellspacing="0" width="100%">
                <thead>
                    <th>Jméno</th>
                    <th>Role</th>
                    <th>E-mail</th>
                    <th></th>
                </thead>
            </table>
        </div>
    </div>
</div>


