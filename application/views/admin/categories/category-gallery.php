<div class="row">
    <div class="col-md-6">
        <h4 class="m-b-lg">Category Gallery</h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" formaction="<?php echo base_url(); ?>admin/categories/"
                class="btn btn-sm btn-primary click-action text-right"><i class="fa fa-angle-left"></i> &nbsp; Zpět
        </button>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Upload New Images</h4>
            <?php $uploadUrl = base_url() . "admin/categories/upload/{$category->category_id}"; ?>
            <?php echo form_open('', array(
                'class' => 'dropzone',
                'data-plugin' => 'dropzone',
                'data-options' => "{ autoDiscover : false, url: '" . $uploadUrl . "'}"
            )) ?>
            <div class="dz-message">
                <h3 class="m-h-lg">Drop files here or click to upload.</h3>
                <p class="m-b-lg text-muted">(This is just a demo dropzone. Selected files are not actually
                    uploaded.)</p>
            </div>
            </form>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="mail-toolbar m-b-lg">
            <h4 class="m-b-lg">
                Category Images
                <a href="<?php echo base_url(); ?>admin/categories/gallery/<?php echo $category->category_id ?>"
                   class="btn btn-default"><i class="fa fa-refresh"></i></a>
            </h4>
        </div>
    </div>
</div>
<?php if ($category->photos): ?>

    <!-- Image Gallery -->
    <div class="gallery row">
        <?php foreach ($category->photos as $photo): ?>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <div class="gallery-item">
                    <div class="thumb">
                        <a href="<?php echo $photo->photo_file ?>" data-lightbox="gallery-2"
                           data-title="<?php echo $photo->photo_title ?>">
                            <img class="img-responsive" src="<?php echo $photo->photo_file ?>"
                                 alt="<?php echo $photo->photo_title ?>">
                        </a>
                    </div>
                    <div class="caption">
                        <?php echo form_open() ?>
                            <input type="hidden" name="photo_id" value="<?php echo $photo->photo_id ?>">
                            <button type="submit" class="btn btn-danger rounded btn-xs"><span class="fa fa-remove"></span></button>
                        </form>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div><!-- END .gallery -->
<?php else: ?>
    <div class="alert alert-warning">
        <div>No existing images, please use the dropzone above to upload new images.</div>
    </div>
<?php endif; ?>