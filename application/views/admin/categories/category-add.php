<div class="row">
    <div class="col-md-6">
        <h4 class="m-b-lg">Správa uživatelů</h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" formaction="<?php echo base_url(); ?>admin/categories/" class="btn btn-sm btn-primary click-action text-right"><i class="fa fa-angle-left"></i> &nbsp; Zpět</button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Přidat nového uživatele</h4>

            <?php echo form_open() ?>
                <div class="table-responsive">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="parent_id" >Category</label>

                            <select name="parent_id" class="form-control">
                                <option value="">select category</option>
                                <?php
                                foreach($categoriesList as $cat)
                                {
                                    $cat_id = key($cat);
                                    $cat_name = $cat[$cat_id];
                                    $selected = ($cat_id == $this->input->post('category_id')) ? ' selected="selected"' : "";
                                    echo '<option value="'.$cat_id.'" '.$selected.'>'.$cat_name.'</option>';
                                }
                                ?>
                            </select>

                        </div>
                        <button type="submit" name="submit_add_category" value="1" class="btn btn-primary btn-md">Uložit</button>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="category_name" >Category Name *</label>
                            <input type="text" name="category_name"
                                   value="<?php echo $this->input->post('category_name'); ?>" class="form-control" id="category_name" />
                            <span class="text-danger"><?php echo form_error('category_name');?></span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>