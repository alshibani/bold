<div class="row">
    <div class="col-md-6">
        <h4 class="m-b-lg">Detail uživatele</h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" formaction="<?php echo base_url(); ?>admin/users/" class="btn btn-sm btn-primary click-action text-right"><i class="fa fa-angle-left"></i> &nbsp; Zpět</button>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="widget p-lg">
            
            <div class="media">
                <div class="media-left">
                    <div class="avatar avatar-lg avatar-circle">
                        <img class="img-responsive" src="http://via.placeholder.com/150" alt="avatar">
                    </div><!-- .avatar -->
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo $user->first_name.' '.$user->last_name ?></h4>
                    <small class="media-meta"><?php echo $user->email ?></small>
                    <small class="media-meta text-warning"><?php echo $user->role ?></small>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-8">
        <div class="widget p-lg">
        <h4 class="m-b-lg">Detail uživatele</h4>
        <div class="table-responsive">
            <?php echo form_open() ?>
                <div class="form-group">
                    <label for="fnameinput">Jméno</label>
                    <input name="first_name" type="text" class="form-control" id="fnameinput" placeholder="First name" value="<?php echo $user->first_name ?>">
                </div>
                <div class="form-group">
                    <label for="lnameinput">Příjmení</label>
                    <input name="last_name" type="text" class="form-control" id="lnameinput" placeholder="Last name" value="<?php echo $user->last_name ?>">
                </div>
                <div class="form-group">
                    <label for="emailinput">Email</label>
                    <input name="email" type="email" class="form-control" id="emailinput" placeholder="Email" value="<?php echo $user->email ?>">
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <select name="role" id="role" class="form-control">
                        <?php 
                            foreach($roles as $role_key => $role) {
                                if($user->roleid == $role['id']) {
                                    echo '<option value="'.$role_key.'" selected>'.$role.'</option>';
                                } else {
                                    echo '<option value="'.$role_key.'">'.$role.'</option>';
                                }
                            } 
                        ?>
                    </select>
                </div>
                <button type="submit" name="submit_edit_user" value="1" class="btn btn-primary btn-md">Uložit změny</button>
            </form>
        </div>
    </div>
</div>
