<div class="row">
    <div class="col-md-6">
        <h4 class="m-b-lg">Správa uživatelů</h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" formaction="<?php echo base_url(); ?>admin/users/" class="btn btn-sm btn-primary click-action text-right"><i class="fa fa-angle-left"></i> &nbsp; Zpět</button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Přidat nového uživatele</h4>

            <?php echo form_open() ?>
                <div class="table-responsive">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="fnameinput">Jméno</label>
                            <input name="first_name" type="text" class="form-control" id="fnameinput" placeholder="Jméno">
                        </div>
                        <div class="form-group">
                            <label for="emailinput">E-mail</label>
                            <input name="email" type="email" class="form-control" id="emailinput" placeholder="E-mail" required>
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select name="role" id="role" class="form-control" required>
                                <?php 
                                    foreach($roles as $role_key => $role) {
                                        echo '<option value="'.$role_key.'">'.$role.'</option>';
                                    } 
                                ?>
                            </select>
                        </div>
                        <button type="submit" name="submit_add_user" value="1" class="btn btn-primary btn-md">Uložit</button>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lnameinput">Příjmení</label>
                            <input name="last_name" type="text" class="form-control" id="lnameinput" placeholder="Příjmení">
                        </div>
                        <div class="form-group">
                            <label for="pwinput">Heslo</label>
                            <input name="password" type="password" class="form-control" id="pwinput" placeholder="Heslo">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>