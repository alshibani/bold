<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Brands List</h4>
    </div>
</div>
<?php
//var_export($brands);exit;
?>

<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Brands List</h4>

            <button type="button" formaction="<?php echo base_url(); ?>admin/Brands/add/" class="btn btn-primary click-action">Add </button>

            <div class="space-25"></div>

            <table id="responsive-datatable" data-plugin="DataTable" data-options="{
                        ajax: base_url+'admin/brands/brands_json',
                        responsive: true,
                        keys: true,
                        order: false,
                        columnDefs: [
                            {
                                'render': function ( data, type, row ) {
                                    return '<button type=\'button\' formaction=\''+base_url+'admin/categories/detail/'+data+'\' class=\'btn btn-primary btn-xs edit-category click-action\'>Edit</button>'
                                            + ' <button type=\'button\' formaction=\''+base_url+'admin/categories/delete/'+data+'\' class=\'btn btn-danger btn-xs edit-category click-action-confirm\'>Delete</button>';
                                },
                                'targets': 1
                            },
                            {
                                'render': function ( data, type, row ) {
                                    return '<a href=\''+base_url+'admin/categories/detail/'+row[2]+'\'>'+data+'</a>';
                                },
                                'targets': 0
                            }
                        ],
                        aoColumns: [ 
                            {'sClass': 'text-left'},
                            {'sClass': 'text-left'},
                            {'sClass': 'text-right'}
                        ],
                        language: {
                            'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/English.json'
                        }
                    }" class="table" cellspacing="0" width="100%">
                <thead>
                    <th>Name</th>
                    <th></th>
                </thead>
            </table>
        </div>
    </div>
</div>


