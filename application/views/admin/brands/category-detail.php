<div class="row">
    <div class="col-md-6">
        <h4 class="m-b-lg">Category Details</h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" formaction="<?php echo base_url(); ?>admin/categories/"
                class="btn btn-sm btn-primary click-action text-right"><i class="fa fa-angle-left"></i> &nbsp; Back
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="widget p-lg">

            <div class="media">
                <div class="media-body">
                    <h4 class="media-meta"><?php echo $category->parent_name ?></h4>
                    <h4 class="media-heading"><?php echo $category->category_name ?></h4>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Detail uživatele</h4>
            <div class="table-responsive">
                <?php echo form_open() ?>
                <div class="form-group">
                    <label for="parent_id">Category</label>

                    <select name="parent_id" class="form-control">
                        <option value="">select category</option>
                        <?php
                        foreach ($categoriesList as $cat) {
                            $cat_id = key($cat);
                            $cat_name = $cat[$cat_id];
                            $selected = ($cat_id == $category->parent_id) ? ' selected="selected"' : "";
                            echo '<option value="' . $cat_id . '" ' . $selected . '>' . $cat_name . '</option>';
                        }
                        ?>
                    </select>

                </div>
                <div class="form-group">
                    <label for="category_name">Category Name *</label>
                    <input type="text" name="category_name"
                           value="<?php echo($this->input->post('category_name') ? $this->input->post('category_name') : $category->category_name); ?>"
                           class="form-control" id="category_name"/>
                    <span class="text-danger"><?php echo form_error('category_name'); ?></span>
                </div>

                <button type="submit" name="submit_edit_category" value="1" class="btn btn-primary btn-md">Save changes
                </button>
                </form>
            </div>
        </div>
    </div>
