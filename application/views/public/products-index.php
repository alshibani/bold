<div class="col-md-12">
    <h4 class="m-b-lg">Products List</h4>
</div><!-- END column -->

<div class="col-md-12">

    <?php if ($products): ?>
        <?php foreach ($products as $product): ?>
<?php //var_export($product);exit;?>
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title"><?php echo $product['product_name']?></h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="media">
                        <div class="media-left">
                            <div class="icon icon-circle m-0 m-r-md b-0 primary text-white" style="width: 90px; height: 90px; line-height: 90px;">
                                <?php if($product['photo_file']): ?>
                                    <img src="<?php echo base_url('uploads/') . $product['photo_file'] ?>">
                                <?php endif;?>
                               </div>
                        </div>
                        <div class="media-body p-b-md p-t-xs">
                            <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quaerat repellendus est, voluptate cupiditate, iusto!</p>
                        </div>
                    </div><!-- .media -->
                </div><!-- .widget-body -->
            </div><!-- .widget -->

        <?php endforeach; ?>
    <?php endif; ?>

</div>