<div class="col-md-12">
    <h4 class="m-b-lg">Products List</h4>
</div><!-- END column -->

<div class="col-md-12">
    <div class="widget p-lg">
        <p class="m-b-lg docs">&nbsp;</p>

        <table class="table table-striped">
            <tr>
                <th>#</th>
                <td><?php echo $product->product_id ?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?php echo $product->product_name ?></td>
            </tr>
            <tr>
                <th>Category</th>
                <td><?php echo $product->category_name ?></td>
            </tr>
            <tr>
                <th>Description</th>
                <td><?php echo nl2br($product->product_descrption) ?></td>
            </tr>
            <tr>
                <th>Location In Warehouse</th>
                <td><?php echo nl2br($product->location) ?></td>
            </tr>
            <tr>
                <th>Purchase Price</th>
                <td><?php echo $product->purchase_price ?></td>
            </tr>
            <tr>
                <th>Wholesale Price</th>
                <td><?php echo $product->wholesale_price ?></td>
            </tr>
            <tr>
                <th>Retail Price</th>
                <td><?php echo $product->retail_price ?></td>
            </tr>

            <tr>
                <th>EAN</th>

                <td><?php echo $product->ean ?></td>
            </tr>
            <tr>
                <th>Items in Stock</th>

                <td><?php echo $product->items_in_stock ?></td>
            </tr>
            <tr>
                <th>Created</th>

                <td><?php echo $product->create_date ?></td>
            </tr>
            <tr>
                <th>Last Update</th>
                <td><?php echo $product->update_date ?></td>
            </tr>
            <?php if ($product->photos): ?>
                <tr>
                    <th>Gallery</th>
                    <td>
                        <div class="gallery row">
                            <?php foreach ($product->photos as $photo): ?>
                                <div class="col-xs-6 col-sm-4 col-md-3">
                                    <div class="gallery-item">
                                        <div class="thumb">
                                            <a href="<?php echo base_url('uploads/') . $photo->photo_file ?>"
                                               data-lightbox="gallery-2"
                                               data-title="<?php echo $photo->photo_title ?>">
                                                <img class="img-responsive"
                                                     src="<?php echo base_url('uploads/') . $photo->photo_file ?>"
                                                     alt="<?php echo $photo->photo_title ?>">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div><!-- END .gallery -->
                    </td>
                </tr>
            <?php endif; ?>
        </table>
    </div><!-- .widget -->
</div><!-- END column -->