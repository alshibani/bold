<html>
<head>
<style>
body, a{
    background: #333;
    color: #fff;
}
.link{
    padding: 8px;
    border-radius: 4px;
    margin-top: 10px;
    float: right;
    margin-right: 10px;
    padding-left: 3px;
    padding-right: 3px;
}
.link a{
    text-decoration: none;
    background: #444;
    color: #fff;
    padding: 6px;
    border-radius: 4px;
    margin: 3px;
    transition: all .3s ease;
}
.link a:hover{
    background: #fff;
    color: #000;
}
.userinfo{
    float: left;
    margin-left: 10px;
    margin-top: 10px;
}
</style>
</head>
<body>

    <h1>Frontend</h1>
<?php if($this->ion_auth->user()->row()){ ?>
    <span class="userinfo">Logged in as: <?php echo $this->ion_auth->user()->row()->email; ?></span>
<?php } ?>  

<span class="link">
<?php if($this->ion_auth->in_group(3)) { ?>
<a href="platform/">EMPLOYEE ADMIN</a>
<?php } elseif($this->ion_auth->in_group(4)) { ?>
<a href="platform/">EMPLOYER ADMIN</a>
<?php } ?>
</span>

<!-- <img src="http://placekitten.com/g/300/300" id="catpic"> -->

</body>
</html>