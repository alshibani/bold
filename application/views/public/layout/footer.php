
</section>
</div>

  <!-- APP FOOTER -->
  <div class="wrap p-t-0">

  </div>
  <!-- /#app-footer -->

</main>

	<!-- build:js ../assets/js/core.min.js -->
	<script src="<?php echo base_url(); ?>assets/libs/bower/jquery/dist/jquery-2.2.4.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bower/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bower/jQuery-Storage-API/jquery.storageapi.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bower/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bower/PACE/pace.min.js"></script>
	<!-- endbuild -->

	<script src="<?php echo base_url(); ?>assets/js/bold.js"></script>

	<!-- build:js ../assets/js/app.min.js -->
	<script src="<?php echo base_url(); ?>assets/js/library.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/app.js"></script>


	<!-- endbuild -->
	<script src="<?php echo base_url(); ?>assets/libs/bower/moment/moment.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bower/fullcalendar/dist/fullcalendar.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/fullcalendar.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/misc/datatables/datatables.min.js"></script>
</body>
</html>