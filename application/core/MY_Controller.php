<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
}

class Admin_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        
        if($this->ion_auth->in_group(3) || $this->ion_auth->in_group(4)) {
            redirect(base_url().'platform/');
        }
    }
}

class Front_admin_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        
        if($this->ion_auth->in_group(1) || $this->ion_auth->in_group(2)) {
            redirect(base_url().'admin/');
        }
    }
}

class Public_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }
}