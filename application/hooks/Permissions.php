<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions{
    
    private $ci;
    public function __construct(){
        $this->ci =& get_instance();
    }

    public function Check(){
        $user_session_data = $this->ci->session->userdata();
        if((!isset($user_session_data['identity'])) AND (!isset($user_session_data['user_id']))){
            if($this->ci->router->directory == "admin/" && $this->ci->router->fetch_class() != "login"){
                redirect('admin/login');
            }
        }   
    }
    
}